import React from 'react';
import { makeStyles } from '@material-ui/core/styles';

import { Paper, Grid, Typography} from '@material-ui/core';
import TeacherPicture from '../../assets/profile.jpg'

const useStyles = makeStyles((theme) => ({
   
    paper: {
      padding: theme.spacing(2),
      textAlign: 'center',
      boxShadow: '0 0 28px rgb(0 0 0 / 8%)',
      borderRadius: '10px',
    },

    teacherHeading: {
      fontSize: '18px',
      fontWeight: '600',
      color: '#1b3133',
    },

    teacherIdentity: {
      fontSize: '14px',
      lineHeight: '1.71rem',
      color: '#6c757d',
    },
  }));

export default function TeacherIdentity() {

  const classes = useStyles();
  return (
    <Grid container alignItems="center">
         <Grid item xs={12} >
             <Paper className={classes.paper} style={{height: '250px'}}>
                <img src={TeacherPicture} alt="" style={{height: '160px', width: '160px'}}/>
                <Typography className={classes.teacherHeading} variant="h5">
                    Mrs Teacher
                </Typography>
                <Typography className={classes.teacherIdentity} variant="body1">
                    Assistant Profession, Department of ICE, Noakhali Science & Technology University
                </Typography>
             </Paper>
        </Grid>
    </Grid>
  );
}