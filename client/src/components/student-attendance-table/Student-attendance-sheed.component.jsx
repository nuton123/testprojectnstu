import React, { useState } from "react";
import MaterialTable from "@material-table/core";
import { makeStyles, Button, Container, } from "@material-ui/core";
import { tableIcons } from "../../lib/material-table-config";

const useStyles = makeStyles((theme) => ({
  tableStyle: {
    boxShadow: "0 0 28px rgb(0 0 0 / 8%)",
    zIndex:"2"
  },

  button: {
    width: "30px",
    height: "40px",
    borderRadius: "50px",
    textTransform: "uppercase",
    fontWeight: "800",
    margin: "5px",
  },
  upperGrid: {
    display: "flex",
    position: "sticky",
    top: "0",
    zIndex:"5",
  },
}));

export default function StudentAttendanceSheet() {
  const classes = useStyles();
  const [selectedRow, setSelectedRow] = useState(null);

  return (
    <Container>
      <MaterialTable
        className={classes.tableStyle}
        style={{ borderRadius: "10px" }}
        title="Students Records for Course Teacher"
        icons={tableIcons}
        columns={[
          {
            title: "Student Photo",
            field: "photo",
            render: (rowData) => (
              <img src={rowData.photo} alt="" style={{ width: "50%" }} />
            ),
          },
          { title: "Student ID", field: "studentId" },
          { title: "Student Name", field: "studentName" },
          { title: "Session", field: "session" },
          { title: "Hall Name", field: "hallName" },
          {
            title: "Status",
            field: "button",
            render: (rowData) =>
              rowData && (
                <>
                  <Button
                    variant="contained"
                    color="secondary"
                    size="small"
                    className={classes.button}
                  >
                    A
                  </Button>

                  <Button
                    variant="contained"
                    color="primary"
                    size="small"
                    className={classes.button}
                  >
                    P
                  </Button>
                </>
              ),
          },
        ]}
        data={[
          {
            studentId: "2050608001",
            studentName: "Student 1",
            session: "2016-2017",
            hallName: "ASH",
            photo: "https://www.w3schools.com/w3images/avatar2.png",
          },
          {
            studentId: "2050608002",
            studentName: "Student 2",
            session: "2016-2017",
            hallName: "ASH",
            photo:
              "https://www.pngkey.com/png/full/114-1149878_setting-user-avatar-in-specific-size-without-breaking.png",
          },
          {
            studentId: "2050608003",
            studentName: "Student 3",
            session: "2016-2017",
            hallName: "ASH",
            photo:
              "https://www.pngkey.com/png/full/114-1149878_setting-user-avatar-in-specific-size-without-breaking.png",
          },
          {
            studentId: "2050608004",
            studentName: "Student 4",
            session: "2016-2017",
            hallName: "ASH",
            photo:
              "https://www.pngkey.com/png/full/114-1149878_setting-user-avatar-in-specific-size-without-breaking.png",
          },
          {
            studentId: "2050608005",
            studentName: "Student 5",
            session: "2016-2017",
            hallName: "ASH",
            photo:
              "https://www.pngkey.com/png/full/114-1149878_setting-user-avatar-in-specific-size-without-breaking.png",
          },
          {
            studentId: "2050608006",
            studentName: "Student 6",
            session: "2016-2017",
            hallName: "ASH",
            photo:
              "https://www.pngkey.com/png/full/114-1149878_setting-user-avatar-in-specific-size-without-breaking.png",
          },
          {
            studentId: "2050608007",
            studentName: "Student 7",
            session: "2016-2017",
            hallName: "ASH",
            photo:
              "https://www.pngkey.com/png/full/114-1149878_setting-user-avatar-in-specific-size-without-breaking.png",
          },
        ]}
        onRowClick={(evt, selectedRow) =>
          setSelectedRow(selectedRow.tableData.id)
        }
        options={{
          pageSize: 20,
          rowStyle: (rowData) => ({
            backgroundColor:
              selectedRow === rowData.tableData.id ? "#EEE" : "#FFF",
          }),
        }}
      />
    </Container>
  );
}
