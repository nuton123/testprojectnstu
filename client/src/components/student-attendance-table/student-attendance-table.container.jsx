import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';

import { selectTeacherIsPending } from '../../redux/teacher/teacher.selectors';
import WithSpinner from '../with-spinner/With-spinner.component';
import StudentAttendanceTable from './student-attendance-table.component';

//here we need to name 'isLoading' exact same as our component 'WithSpinner' expects
const mapStateToProps = createStructuredSelector(
    {
        isLoading: selectTeacherIsPending
    }
);

const StudentAttendanceTableContainer = compose(
    connect(mapStateToProps),
    WithSpinner
)(StudentAttendanceTable);

export default StudentAttendanceTableContainer;