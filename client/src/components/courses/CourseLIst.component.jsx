import React from "react";
import { Container, Grid } from "@material-ui/core";
import { makeStyles } from "@material-ui/core";
import courseData from "./coursedata";
import ContentCard from './ContentCard';


const useStyles = makeStyles(() => ({
  root: {},
  centerGrid: {
    width: "100%",
    margin: "0 auto",
  },
  paper1: {
    minWidth: "500px",
    margin: "50px auto",
  },
  headingTop: {
    display: "flex",
    justifyContent: "center",
    paddingTop: "50px",
  },
  courseCard: {
    maxWidth: 275,
    marginTop: "30px",
  },
}));

const CourseList = () => {
  const classes = useStyles();

  /**  const [courses, setCourses] = useState([]);

  useEffect(() => {
    loadCoures();
  }, []);

  const loadCoures = () => {
    const result = axios.get("http://localhost:3003/courses");
    setCourses(result.data.reverse());
  };
 */
  
  return (
    <Container className={classes.container}>
        <Grid container>
        {courseData.map((a, index) => (
          <Grid item key={index}>
            <ContentCard
              departmentName={a.departmentName}
              courseName={a.courseName}
              courseCode={a.courseCode}
              batch={a.batch}
              courseStartDate={a.courseStartDate}

            />
          </Grid>
        ))}
        </Grid>
    </Container>
  );
};

export default CourseList;
