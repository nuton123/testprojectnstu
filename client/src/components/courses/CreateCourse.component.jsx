import React, { useState } from "react";
// import axios from 'axios';
import { Container, Grid, Button, Typography } from "@material-ui/core";
import TextField from "@material-ui/core/TextField";
import { makeStyles } from "@material-ui/core";
import AddBoxIcon from "@material-ui/icons/AddBox";
import CourseLIst from "./CourseLIst.component";

const useStyles = makeStyles(() => ({
  root: {},
  centerGrid: {
    width: "100%",
    margin: "0 auto",
  },
  paper1: {
    minWidth: "500px",
    margin: "50px auto",
  },
  headingTop: {
    display: "flex",
    justifyContent: "center",
    paddingTop: "50px",
  },
  courseCard: {
    maxWidth: 275,
    marginTop: "30px",
  },
}));

const CreateCourese = () => {
  const classes = useStyles();

  const [course, setCourses] = useState({
    departmentName: "",
    courseName: "",
    courseCode: "",
    batch: "",
    coursStartDate: "",
  });

  const { departmentName, courseName, courseCode, batch, courseStartDate } = course;

  const onInputChange = (e) => {
      e.preventDefault();
    setCourses({ ...course, [e.target.name]: e.target.value });
  };

  // const onSubmit = async (e) => {
  //   await axios.post("http://localhost:3004/courses", course);
  // };

  return (
    <Container>
      <Grid className={classes.headingTop}>
        <AddBoxIcon />
        <Typography variant="h5">Add Courses</Typography>
      </Grid>
      <Grid item sx={12} className={classes.centerGrid}>
        <form
          className={classes.root}
          noValidate
          autoComplete="off"
          // onSubmit={(e) => onSubmit(e)}
        >
          <div>
            <TextField
              required
              id="outlined-required"
              label="Department Name"
              variant="outlined"
              name="departmentName"
              value={departmentName}
              onChange={(e) => onInputChange(e)}
            />
            <TextField
              required
              id="outlined-required"
              label="Course Name"
              variant="outlined"
              name="courseName"
              value={courseName}
              onChange={(e) => onInputChange(e)}
            />
            <TextField
              required
              id="outlined-required"
              label="Course Code"
              variant="outlined"
              name="courseCode"
              value={courseCode}
              onChange={(e) => onInputChange(e)}
            />
            <TextField
              required
              id="outlined-required"
              label="Batch"
              type="number"
              variant="outlined"
              name="batch"
              value={batch}
              onChange={(e) => onInputChange(e)}
            />
            <TextField
              id="date"
              label="Course Start Date"
              type="date"
              variant="outlined"
              defaultValue="2017-05-24"
              InputLabelProps={{
                shrink: true,
              }}
              name="courseStartDate"
              value={courseStartDate}
              onChange={(e) => onInputChange(e)}
            />
          </div>
          <Button variant="contained" color="primary">
            Add Course
          </Button>
        </form>
      </Grid>

      <Grid>
      <CourseLIst />

      </Grid>

    </Container>
  );
};

export default CreateCourese;
