import React from 'react';
import { makeStyles, MenuList, MenuItem, Typography } from '@material-ui/core';
import { useHistory, useLocation,useRouteMatch   } from 'react-router-dom';
import { AccountCircleOutlined, 
    ImportContactsOutlined, 
    ErrorOutlined     
} from '@material-ui/icons';

const useStyles = makeStyles(() => ({
    menuItemRoot: {
        "&$menuItemSelected, &$menuItemSelected:focus, &$menuItemSelected:hover": {
            backgroundColor: "#2D46B9",
            margin: '0 8px',
            borderRadius: '4px'
        },
        color: '#fff',
        padding: '15px',
        fontSize: '16px',
        fontWeight: '300',
        letterSpacing: '0.03em',
    },
    menuItemSelected: {},
    heading: {
        color: '#fff',
        fontSize: '1.5rem',
        fontWeight: '700',
        padding: '10px 0 20px 0',
        textAlign: 'center',
    }
}));

function TeacherSidebar() {
    const classes = useStyles();
    let match = useRouteMatch();
    let location = useLocation();
    let history = useHistory();
  
    return (
        <MenuList>
            <Typography variant='h4' className={classes.heading}>
                Menu
            </Typography>
            <MenuItem
             classes={{ 
                root: classes.menuItemRoot,
                selected: classes.menuItemSelected
             }}
             onClick={ () =>  history.push(`${match.url}/dashboard`) }
             selected={location.pathname === `${match.url}/dashboard`}   
            ><AccountCircleOutlined />&nbsp; Dashboard</MenuItem>
            <MenuItem
             classes={{ 
                root: classes.menuItemRoot,
                selected: classes.menuItemSelected
             }}
             onClick={ () =>  history.push(`${match.url}/profile`) }
             selected={location.pathname === `${match.url}/profile`}   
            ><AccountCircleOutlined />&nbsp; Profile</MenuItem>
               <MenuItem
             classes={{ 
                root: classes.menuItemRoot,
                selected: classes.menuItemSelected
             }}
             onClick={ () => history.push(`${match.url}/teacher-validation-request`) }
             selected={location.pathname === `${match.url}/teacher-validation-request`}  
             ><ImportContactsOutlined />&nbsp; Teacher Validation Request</MenuItem>
                <MenuItem
             classes={{ 
                root: classes.menuItemRoot,
                selected: classes.menuItemSelected
             }}
             onClick={ () => history.push(`${match.url}/student-validation-request`) }
             selected={location.pathname === `${match.url}/student-validation-request`}  
             ><ImportContactsOutlined />&nbsp; Student Validation Request</MenuItem>
            <MenuItem
             classes={{ 
                root: classes.menuItemRoot,
                selected: classes.menuItemSelected
             }}
             onClick={ () => history.push(`${match.url}/add-student`) }
             selected={location.pathname === `${match.url}/attendance`}  
             ><ImportContactsOutlined />&nbsp; Add Student</MenuItem>
            <MenuItem
             classes={{ 
                root: classes.menuItemRoot,
                selected: classes.menuItemSelected
             }}
             onClick={ () => history.push(`${match.url}/add-teacher`) }
             selected={location.pathname === `${match.url}/add-teacher`} 
             ><ErrorOutlined />&nbsp; Add Teacher</MenuItem>
            
        </MenuList>
    );
};

export default TeacherSidebar;
