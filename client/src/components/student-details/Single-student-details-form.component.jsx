import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import { Grid, Box, Container } from "@material-ui/core";
import { Typography } from "@material-ui/core";
import Logo from "../../assets/nstu-logo.png";

import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    boxSizing: "borderBox",
    padding: "20px",
    background: "skyblue",
    width:"80%",
    margin:"0 auto",
  },
  paper: {
    padding: theme.spacing(2),
    color: theme.palette.text.secondary,
  },
  studentIdentity: {
    marginTop: "50px",
    width: "70%",
    margin: "0 auto",
    display: "flex",
    flexDirection: "column",
    alignItems: "left",
  },
  childRow: {
    display: "flex",
    flexDirection: "row",
    
  },
  lastChild: {
    marginTop: "30px",
    width: "70%",
    margin: "0 auto",
    display: "flex",
    flexDirection: "row",
  },
  bottomGrid:{
    marginTop:"30px",
    width:"70%",
    margin:"0 auto",
    display:'flex',
    justifyContent:'space-between',
    marginBottom:"40px",

  },
  flexDiv1: {
    width: "70%",
    margin: "0 auto",
    display: "flex",
    flexDirection: "row",
  },
  table: {
    maxWidth: "80%",
    margin: "0 auto",
    "& .MuiTableCell-root": {
      border: "1px solid black",
    },
    marginTop: "30px",
  },
}));

function createData(sl, courseCode, stipulatdNo, classesHeld, attend, remarks) {
  return { sl, courseCode, stipulatdNo, classesHeld, attend, remarks };
}

const rows = [
  createData(1, "ICE-4201", 6.0, 24, 4.0, 0),
  createData(2, "ICE-4202", 9.0, 37, 4.3, 0),
  createData(3, "ICE-4203", 16.0, 24, 6.0, 0),
  createData(4, "ICE-4204", 3.7, 67, 4.3, 0),
  createData(5, "ICE-4205", 16.0, 49, 3.9, 0),
];

const SingleStudentDetailsForm = () => {
  const classes = useStyles();
  return (
    <Container className={classes.root}>
      <Grid container spacing={2}>
        <Grid item xs={3}>
          <img
            src={Logo}
            alt={"Logo"}
            style={{ height: "150px", width: "auto" }}
          />
        </Grid>
        <Grid item xs={6}>
          <Typography variant="h5">
            Noakhali Science And Technology University
          </Typography>
          <Typography variant="h5">Exam Entry Form</Typography>
          <div className={classes.flexDiv1}>
            <Typography variant="h6">
              Year: ..............
            </Typography>
            <Typography variant="h6">Session: .........</Typography>
          </div>
          <div className={classes.flexDiv1}>
            <Typography variant="h6">Term: .............</Typography>
            <Typography variant="h6">Examination: ..............</Typography>
          </div>
        </Grid>
        <Grid item xs={3}>
          <Box
            sx={{ width: 130, height: 130, p: 2, border: "1px dashed grey" }}
          >
            <Typography align="center">Stump Size Photograph</Typography>
          </Box>
        </Grid>
      </Grid>

      <Grid container className={classes.studentIdentity}>
        <Grid item xs className={classes.childRow}>
          <Typography align="left" mr={2}>
            Name
          </Typography>
          <Box
            component="span"
            sx={{ width: "100%", height: 10, p: 2, border: "1px dashed grey" }}
          >
            <Typography align="left">Nuton Chakma</Typography>
          </Box>
        </Grid>
        <Grid item xs className={classes.childRow}>
          <Typography align="left">Fathers Name</Typography>
          <Box
            component="span"
            sx={{ width: "70%", height: 10, p: 2, border: "1px dashed grey" }}
          >
            <Typography align="left">This is father name</Typography>
          </Box>
        </Grid>
        <Grid item xs className={classes.childRow}>
          <Typography align="left">Mothers Name</Typography>
          <Box
            component="span"
            sx={{ width: "70%", height: 10, p: 2, border: "1px dashed grey" }}
          >
            <Typography align="left">This is mother name</Typography>
          </Box>
        </Grid>
        <Grid item xs className={classes.childRow}>
          <Typography align="left">Department</Typography>
          <Box
            component="span"
            sx={{ width: "70%", height: 10, p: 2, border: "1px dashed grey" }}
          >
            <Typography align="left">This is departmentname name</Typography>
          </Box>
        </Grid>
        <Grid item xs className={classes.childRow}>
          <Typography align="left">Roll No</Typography>
          <Box
            component="span"
            sx={{ width: "70%", height: 10, p: 2, border: "1px dashed grey" }}
          >
            <Typography align="left">ASH1711065M</Typography>
          </Box>
        </Grid>
        <Grid item xs className={classes.childRow}>
          <Typography>Name of The Hall</Typography>
          <Box
            component="span"
            sx={{ width: 300, height: 10, p: 2, border: "1px dashed grey" }}
          >
            <Typography>ASH</Typography>
          </Box>
        </Grid>
        <Grid item xs className={classes.childRow}>
          <Typography>Date of commencement of Examination</Typography>
          <Box
            component="span"
            sx={{ width: 300, height: 10, p: 2, border: "1px dashed grey" }}
          >
            <Typography>This is something</Typography>
          </Box>
        </Grid>
      </Grid>

      <Grid>
        <TableContainer>
          <Table className={classes.table} aria-label="simple table">
            <TableHead>
              <TableRow>
                <TableCell>SL</TableCell>
                <TableCell align="center">Course Code</TableCell>
                <TableCell align="center">Stipulated No of Classes</TableCell>
                <TableCell align="center">Number of Classes held</TableCell>
                <TableCell align="center">Number of classes attended</TableCell>
                <TableCell align="center">Remarks</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {rows.map((row) => (
                <TableRow key={row.sl}>
                  <TableCell component="th" scope="row">
                    {row.sl}
                  </TableCell>
                  <TableCell align="center">{row.courseCode}</TableCell>
                  <TableCell align="center">{row.stipulatdNo}</TableCell>
                  <TableCell align="center">{row.classesHeld}</TableCell>
                  <TableCell align="center">{row.attend}</TableCell>
                  <TableCell align="center">{row.remarks}</TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </TableContainer>
      </Grid>

      <Grid item xs className={classes.lastChild}>
        <Typography>Percentage of Attendacne</Typography>
        <Box
          component="span"
          sx={{ width: 300, height: 10, p: 2, border: "1px dashed grey" }}
        >
          <Typography>80%</Typography>
        </Box>
      </Grid>

      <Grid xs className={classes.bottomGrid}>
        <Grid item>
          <Typography>..........</Typography>
          <Typography>Chairman</Typography>
        </Grid>
        <Grid>
          <Typography>..........</Typography>
          <Typography>Provost</Typography>
        </Grid>
      </Grid>
    </Container>
  );
};

export default SingleStudentDetailsForm;
