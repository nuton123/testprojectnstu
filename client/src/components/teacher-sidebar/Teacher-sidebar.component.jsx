import React from 'react';
import { makeStyles, MenuList, MenuItem, Typography } from '@material-ui/core';
import { useHistory, useLocation,useRouteMatch   } from 'react-router-dom';
import { AccountCircleOutlined, 
    ImportContactsOutlined, 
    ErrorOutlined, PersonOutlined, 
    PermIdentityOutlined 
} from '@material-ui/icons';

// import { connect } from 'react-redux';
// import { createStructuredSelector } from 'reselect';
// import { selectCurrentUser } from '../../redux/user/user.selectors';

const useStyles = makeStyles(() => ({
    menuItemRoot: {
        "&$menuItemSelected, &$menuItemSelected:focus, &$menuItemSelected:hover": {
            backgroundColor: "#2D46B9",
            margin: '0 8px',
            borderRadius: '4px'
        },
        color: '#fff',
        padding: '15px',
        fontSize: '16px',
        fontWeight: '300',
        letterSpacing: '0.03em',
    },
    menuItemSelected: {},
    heading: {
        color: '#fff',
        fontSize: '1.5rem',
        fontWeight: '700',
        padding: '10px 0 20px 0',
        textAlign: 'center',
    }
}));


function TeacherSidebar() {
    const classes = useStyles();
    let match = useRouteMatch();
    let location = useLocation();
    let history = useHistory();
  
    return (
        <MenuList>
            <Typography variant='h4' className={classes.heading}>
                Menu
            </Typography>
            <MenuItem
             classes={{ 
                root: classes.menuItemRoot,
                selected: classes.menuItemSelected
             }}
             onClick={ () =>  history.push(`${match.url}/dashboard`) }
             selected={location.pathname === `${match.url}/dashboard`}   
            ><AccountCircleOutlined />&nbsp; Dashboard</MenuItem>
            <MenuItem
             classes={{ 
                root: classes.menuItemRoot,
                selected: classes.menuItemSelected
             }}
             onClick={ () =>  history.push(`${match.url}/teacher/profile`) }
             selected={location.pathname === `${match.url}/profile`}   
            ><AccountCircleOutlined />&nbsp; Profile</MenuItem>
            <MenuItem
             classes={{ 
                root: classes.menuItemRoot,
                selected: classes.menuItemSelected
             }}
             onClick={ () => history.push(`${match.url}/teacher/attendance`) }
             selected={location.pathname === `${match.url}/attendance`}  
             ><ImportContactsOutlined />&nbsp;Take Attendance</MenuItem>
            <MenuItem
             classes={{ 
                root: classes.menuItemRoot,
                selected: classes.menuItemSelected
             }}
             onClick={ () => history.push(`${match.url}/teacher/student-details`) }
             selected={location.pathname === `${match.url}/student-details`} 
             ><ErrorOutlined />&nbsp; Student Details</MenuItem>
            <MenuItem
             classes={{ 
                root: classes.menuItemRoot,
                selected: classes.menuItemSelected
             }} 
             onClick={ () => history.push(`${match.url}/teacher/co-ordinator`) }
             selected={location.pathname === `${match.url}/co-ordinator`} 
             ><PersonOutlined />&nbsp; Co-ordinator</MenuItem>
            <MenuItem
             classes={{ 
                root: classes.menuItemRoot,
                selected: classes.menuItemSelected
             }}
             onClick={ () => history.push(`${match.url}/teacher/chairman`) }
             selected={location.pathname === `${match.url}/chairman`} 
             ><PermIdentityOutlined />&nbsp; Chairman</MenuItem>
        </MenuList>
    );
};

export default TeacherSidebar;

// const mapStateToProps = createStructuredSelector(
//     {
//         currentUser: selectCurrentUser
//     }
// );

// export default connect(mapStateToProps, null)(withRouter(TeacherSidebar));