import React, { useState } from "react";
import {
  Container,
  Grid,
  Button,
  Select,
  MenuItem,
  Card,
  CardContent,
  Typography,
  TextField,
} from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  Card: {
    margin: "20px 0",
  },
  paper: {
    padding: theme.spacing(1),
    textAlign: "center",
    color: theme.palette.text.secondary,
    display: "flex",
    alignItems: "center",
    margin: "0 auto",
  },

  container: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
  },
  spacingItem: {
    margin: "20px",
  },
  flexItem: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    margin: "5px 0px",
  },
  flexButton: {
    margin: "0px 10px",
  },
  bottomItem: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    margin: "20px 0px",
  },
}));

const VersityToken = () => {
  const classes = useStyles();
  const [formData, setFormData] = useState({
    management: "",
  });

  const handleOnSubmit = (e) => {
    e.preventDefault();
    return alert("success");
  };

  return (
    <Container>
      <Card className={classes.Card}>
        <CardContent>
          <form onSubmit={handleOnSubmit}>
            <Grid container spacing={4} className={classes.container}>

              <Grid>
                <Button
                  type="submit"
                  variant="contained"
                  color="primary"
                  className={classes.spacingItem}
                >
                  Generate Token
                </Button>
              </Grid>
            </Grid>
          </form>

          <Grid item xs={12} className={classes.bottomItem}>
            <TextField variant="outlined" type="texfield" />
            <Button size="small" color="primary" variant="contained">
              Copy
            </Button>
          </Grid>
        </CardContent>
      </Card>

      <Card>
        <CardContent>
          <Grid
            container
            rowSpacing={1}
            columnSpacing={{ xs: 1, sm: 2, md: 3 }}
          >
            <Grid item xs={12}>
              <Typography variant="subtitle1">
                Recent Generated Token
              </Typography>
            </Grid>

            <Grid item xs={12} className={classes.flexItem}>
              <Typography>LUCEJHYV</Typography>
              <Button
                size="small"
                variant="contained"
                color=""
                className={classes.flexButton}
              >
                Copy
              </Button>
              <Button
                size="small"
                variant="contained"
                color="primary"
                className={classes.flexButton}
              >
                Active
              </Button>
            </Grid>

            <Grid item xs={12} className={classes.flexItem} spacing={2}>
              <Typography>LUCEJHYV</Typography>
              <Button
                size="small"
                variant="contained"
                color=""
                className={classes.flexButton}
              >
                Copy
              </Button>
              <Button
                size="small"
                variant="contained"
                color="secondary"
                className={classes.flexButton}
              >
                Expired
              </Button>
            </Grid>
          </Grid>
        </CardContent>
      </Card>
    </Container>
  );
};

export default VersityToken;
