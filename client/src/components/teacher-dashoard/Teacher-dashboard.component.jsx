import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import { Grid, Typography, TextField, Button, Paper } from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  paper: {
    padding: theme.spacing(2),
    textAlign: "center",
    boxShadow: "0 0 28px rgb(0 0 0 / 8%)",
    borderRadius: "10px",
  },

  teacherHeading: {
    fontSize: "18px",
    fontWeight: "600",
    color: "#1b3133",
  },

  StudentIdentity: {
    fontSize: "14px",
    lineHeight: "1.71rem",
    color: "#6c757d",
  },
  verificationForm: {
    width: "500px",
    margin: "0 auto",
  },
}));

const TeacherDashboard = () => {
  const classes = useStyles();

  return (
    <Grid container alignItems="center">
      <Grid item xs={12}>
        <Paper className={classes.paper} style={{ height: "250px" }}>
          <Button variant="contained" color="secondary">Verify Account</Button>
          <form className={classes.verificationForm}>
            <Typography variant="h6" gutterBottom>
              Upload your Id card
            </Typography>
            <TextField fullWidth type="file" required />
            <Button
              type="submit"
              variant="contained"
              color="primary"
              fullWidth
              
            >
              Upload Your Files
            </Button>
          </form>
        </Paper>
      </Grid>
    </Grid>
  );
};

export default TeacherDashboard;
