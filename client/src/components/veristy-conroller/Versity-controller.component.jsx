import React from "react";
import {
  Container,
  Grid,
  Paper,
  TextField,
  Typography,
} from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing(1),
    textAlign: "center",
    color: theme.palette.text.secondary,
  },
}));

const VersityController = () => {
  const classes = useStyles();

  return (
    <Container>
      <Grid container spacing={2} columnSpacing={{ xs: 1, sm: 2, md: 3 }}>
        <Grid item xs={12}>
          <Paper className={classes.paper}>
            <Typography variant="h6">Total Controller</Typography>
            <Typography variant="h6">3</Typography>
          </Paper>
          <Paper className={classes.paper}>
            <TextField label="Token" variant="filled" required fullWidth/>
            <Typography variant="h6">Token left: 2</Typography>
          </Paper>
        </Grid>
      </Grid>
    </Container>
  );
};

export default VersityController;
