import React from "react";
import { Container, Grid, Paper, Typography } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing(1),
    textAlign: "center",
    color: theme.palette.text.secondary,
  },
}));

const AdminDashboard = () => {
  const classes = useStyles();

  return (
    <Container>
      <Grid container spacing={2} columnSpacing={{ xs: 1, sm: 2, md: 3 }}>
        <Grid item xs={4}>
          <Paper className={classes.paper}>
            <Typography variant="h6">Total Validation Requested</Typography>
            <Typography variant="h6">50</Typography>
          </Paper>
        </Grid>
        <Grid item xs={4}>
          <Paper className={classes.paper}>
            <Typography variant="h6">Total Accepted</Typography>
            <Typography variant="h6">30</Typography>
          </Paper>
        </Grid>
        <Grid item xs={4}>
          <Paper className={classes.paper}>
            <Typography variant="h6">Total Rejected</Typography>
            <Typography variant="h6">20</Typography>
          </Paper>
        </Grid>
      </Grid>
      <Grid container spacing={2} columnSpacing={{ xs: 1, sm: 2, md: 3 }}>
        <Grid item xs={4}>
          <Paper className={classes.paper}>
            <Typography variant="h6">Pending VAlidation Requested</Typography>
            <Typography variant="h6">30</Typography>
          </Paper>
        </Grid>
        <Grid item xs={4}>
          <Paper className={classes.paper}>
            <Typography variant="h6">Total Student</Typography>
            <Typography variant="h6">30</Typography>
          </Paper>
        </Grid>
        <Grid item xs={4}>
          <Paper className={classes.paper}>
            <Typography variant="h6">Total Student</Typography>
            <Typography variant="h6">30</Typography>
          </Paper>
        </Grid>
      </Grid>
    </Container>
  );
};

export default AdminDashboard;
