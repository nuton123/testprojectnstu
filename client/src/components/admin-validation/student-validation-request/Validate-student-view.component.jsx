import {
  Container,
  Grid,
  Typography,
  Card,
  makeStyles,
  Button,
} from "@material-ui/core";
import React from "react";
import Logo from "../../../assets/id-card.jpg";

const useStyles = makeStyles({
  grid: {
    display: "flex",
    flexDirection: "row",
    spacing: "2",
  },
  flexGrid: {
    display: "flex",
  },
  card: {
    padding: "20px",
  },
  button: {
    margin: "20px",
  },
});

export default function ValidStudentView() {
  const classes = useStyles();
  return (
    <Container>
      <Grid Container className={classes.grid} spacing={3}>
        <Grid item xs={6}>
          <Card className={classes.card}>
            <Grid className={classes.flexGrid}>
              <Typography variant="h6">Name:</Typography>
              <Typography variant="h6">Nuton Chakma</Typography>
            </Grid>
            <Grid className={classes.flexGrid}>
              <Typography variant="h6">Email:</Typography>
              <Typography variant="h6">nuton121@gmail.com</Typography>
            </Grid>

            <Grid className={classes.flexGrid}>
              <Typography variant="h6">ID:</Typography>
              <Typography variant="h6">Ash1711054</Typography>
            </Grid>
            <Grid className={classes.flexGrid}>
              <Typography variant="h6">Department:</Typography>
              <Typography variant="h6">ICE</Typography>
            </Grid>
            <Grid className={classes.flexGrid}>
              <Typography variant="h6">Session:</Typography>
              <Typography variant="h6">2016-2017</Typography>
            </Grid>
            <Grid>
              <Button variant="contained" className={classes.button}>
                Accept
              </Button>
              <Button variant="contained" className={classes.button}>
                Reject
              </Button>
            </Grid>
          </Card>
        </Grid>
        <Grid item xs={6}>
          <Card>
            <Typography variant="h6">ID Card</Typography>
            <img
              src={Logo}
              alt="id card"
              style={{ height: "auto", width: "450px" }}
            />
          </Card>
        </Grid>
      </Grid>
    </Container>
  );
}
