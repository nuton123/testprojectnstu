import React, { useState } from "react";
import MaterialTable from "@material-table/core";
import { makeStyles, Button, Container } from "@material-ui/core";
import { tableIcons } from "../../../lib/material-table-config";
import VisibilityOutlinedIcon from '@material-ui/icons/VisibilityOutlined';
import { Link } from "react-router-dom";

const useStyles = makeStyles((theme) => ({
  tableStyle: {
    boxShadow: "0 0 28px rgb(0 0 0 / 8%)",
  },

  button: {
    width: "100px",
    height: "40px",
    borderRadius: "50px",
    textTransform: "uppercase",
    fontWeight: "500",
    margin: "5px",
  },
}));

export default function TeacherValidationRequest() {
  const classes = useStyles();
  const [selectedRow, setSelectedRow] = useState(null);

  return (
    <Container>
      <MaterialTable
        className={classes.tableStyle}
        style={{ borderRadius: "10px" }}
        title="Teacaher Validation Request"
        icons={tableIcons}
        columns={[
          {
            title: "Student Photo",
            field: "photo",
            render: (rowData) => (
              <img src={rowData.photo} alt="" style={{ width: "50%" }} />
            ),
          },
          { title: "Teacher ID", field: "teacherId" },
          { title: "Student Name", field: "studentName" },
          {
            title: "Status",
            field: "button",
            cellStyle: {
              minWidth: 450,
            },
            render: (rowData) =>
              rowData && (
                <>
                  <Button
                    variant="contained"
                    size="small"
                    className={classes.button}
                  >
                    pending
                  </Button>

                  <Button
                    variant="contained"
                    size="small"
                    className={classes.button}
                  >
                    Accepted
                  </Button>
                  <Button
                    variant="contained"
                    size="small"
                    className={classes.button}
                  >
                    Rejected
                  </Button>
                </>
              ),
          },
          { title: 'All Details', field: 'button',
              render: (rowData) => rowData && (
                <Button 
                component={Link}
                to="/admin-account/teacher-validation-request/teacher-id1"
                  variant="contained"
                  color="primary"
                  size="small"
                  className={classes.button}
                  startIcon={<VisibilityOutlinedIcon />}
                >
                  View
                </Button>
              )},
        ]}
        data={[
          {
            teacherId: "2050608001",
            studentName: "Teacher 1",

            photo: "https://www.w3schools.com/w3images/avatar2.png",
          },
          {
            teacherId: "2050608002",
            studentName: "Teacher 2",
            photo:
              "https://www.pngkey.com/png/full/114-1149878_setting-user-avatar-in-specific-size-without-breaking.png",
          },
        ]}
        onRowClick={(evt, selectedRow) =>
          setSelectedRow(selectedRow.tableData.id)
        }
        options={{
          rowStyle: (rowData) => ({
            backgroundColor:
              selectedRow === rowData.tableData.id ? "#EEE" : "#FFF",
          }),
        }}
      />
    </Container>
  );
}
