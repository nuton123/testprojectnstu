import React, { useState, useEffect } from "react";
import {
  Container,
  Grid,
  TextField,
  FormControl,
  FormLabel,
  RadioGroup,
  FormControlLabel,
  Radio,
  Button,
  Typography,
  Paper,
} from "@material-ui/core";
import { makeStyles } from "@material-ui/core";
import PasswordValidation from "./Password-Validation.component";

const useStyles = makeStyles((theme) => ({
  paper: {
    padding: "30px",
  },
  radioStyle: {
    display: "flex",
    flexDirection: "row",
    border: "1px solid #C4C4C4",
    borderRadius: "5px",
  },
  genderCaption: {
    marginTop: "12px",
    marginRight: "30px",
    paddingLeft: "15px",
  },
}));

export default function RegistrationForm() {
  const classes = useStyles();
  const [passValues, setPassValues] = useState({
    pass: "",
    confirmPass: undefined,
  });
  const [passValidation, setPassValidation] = useState({
    lowerAndUpperCase: false,
    atleastOneNumberOrSymbol: false,
    atleastEightCharactersLong: false,
  });
  const [formData, setFormData] = useState({
    name: "",
    email: "",
    phone: "",
    gender: "",
    fathersName: "",
    mothersName: "",
    birthDate: "",
    tokenCode: "",
  });

  useEffect(() => {
    const handlePasswordValidation = (pass, confirmPass) => {
      const smallAndUppercase = /(?=.*[a-z])(?=.*[A-Z])/g;
      const atLeastOneNumberOrSymbol = /(?=.*[0-9])|(?=.*[!@#$%^&*])/g;
      const beAtleastEightCharacterLong = /.{8}/g;
      const result = {
        smallAndUpper: smallAndUppercase.test(pass),
        oneNumberOrSymbol: atLeastOneNumberOrSymbol.test(pass),
        atleastEightCharacter: beAtleastEightCharacterLong.test(pass),
        confirmPassMatched: pass === confirmPass,
      };
      return result;
    };

    const passValidationResult = handlePasswordValidation(
      passValues.pass,
      passValues.confirmPass
    );

    setPassValidation((passValidation) => ({
      ...passValidation,
      lowerAndUpperCase: passValidationResult.smallAndUpper,
      atleastOneNumberOrSymbol: passValidationResult.oneNumberOrSymbol,
      atleastEightCharactersLong: passValidationResult.atleastEightCharacter,
      confirmPassMatched: passValidationResult.confirmPassMatched,
    }));
  }, [passValues.pass, passValues.confirmPass]);

  const tokenType = "teacher";

  const handlePassChange = (prop) => (event) => {
    setPassValues({
      ...passValues,
      [prop]: event.target.value,
    });
  };

  const handleOnSubmit = (e) => {
    e.preventDefault();
    if (
      passValidation.atleastEightCharactersLong &&
      passValidation.atleastOneNumberOrSymbol &&
      passValidation.lowerAndUpperCase &&
      passValidation.confirmPassMatched
    ) {
      //TODO: send user info to server and register user
      console.log({ ...formData });
      return alert("success");
    }
    return alert("please meet all requirements");
  };

  return (
    <Container>
      <form onSubmit={handleOnSubmit}>
        <Paper className={classes.paper} elevation={3}>
          <Grid container spacing={4}>
            {tokenType && (
              <Grid item>
                <Typography variant="h4">
                  Register{" "}
                  {tokenType.charAt(0).toUpperCase() + tokenType.slice(1)}
                </Typography>
              </Grid>
            )}
            <Grid item xs={12} className={classes.root}>
              <TextField
                required
                fullWidth
                label="Name"
                placeholder="Enter your name"
                variant="outlined"
                onChange={(e) =>
                  setFormData({ ...formData, name: e.target.value })
                }
              />
            </Grid>
            <Grid item xs={12} className={classes.root}>
              <TextField
                required
                fullWidth
                label="Email"
                placeholder="Enter your email"
                variant="outlined"
                onChange={(e) =>
                  setFormData({ ...formData, email: e.target.value })
                }
              />
            </Grid>
            <Grid item xs={12} className={classes.root}>
              <TextField
                required
                fullWidth
                variant="outlined"
                label="Phone"
                type="tel"
                placeholder="Enter your phone number"
                onChange={(e) =>
                  setFormData({ ...formData, phone: e.target.value })
                }
              />
            </Grid>
            <Grid item xs={12} className={classes.root}>
              <FormControl component="fieldset" className={classes.radioStyle}>
                <FormLabel
                  id="demo-row-radio-buttons-group-label"
                  className={classes.genderCaption}
                >
                  Gender
                </FormLabel>

                <RadioGroup
                  row
                  aria-label="gender"
                  name="gender"
                  style={{ display: "initial" }}
                >
                  <FormControlLabel
                    value="female"
                    control={<Radio required />}
                    label="Female"
                    onChange={(e) =>
                      setFormData({ ...formData, gender: e.target.value })
                    }
                  />
                  <FormControlLabel
                    value="male"
                    control={<Radio required />}
                    label="Male"
                    onChange={(e) =>
                      setFormData({ ...formData, gender: e.target.value })
                    }
                  />
                </RadioGroup>
              </FormControl>
            </Grid>
            <Grid item xs={12} md={6} className={classes.root}>
              <TextField
                required
                fullWidth
                variant="outlined"
                type="password"
                label="Password"
                placeholder="Enter your password"
                onChange={handlePassChange("pass")}
              />
            </Grid>
            <Grid item xs={12} md={6} className={classes.root}>
              <TextField
                required
                fullWidth
                variant="outlined"
                type="password"
                label="Confirm Password"
                placeholder="Enter your confirm password"
                onChange={handlePassChange("confirmPass")}
                disabled={passValues.pass.length <= 0}
              />
            </Grid>
            {passValues.pass.length > 0 && (
              <PasswordValidation passValidation={passValidation} />
            )}
            {tokenType && tokenType === "teacher" && (
              <Grid item xs={12}>
                <TextField
                  variant="outlined"
                  id="outlined-disabled"
                  label="Your department"
                  defaultValue="ICE"
                  placeholder="ICE"
                  fullWidth
                  disabled
                />
              </Grid>
            )}

            {tokenType && tokenType === "student" && (
              <>
                <Grid item xs={12} md={6}>
                  <TextField
                    required
                    fullWidth
                    variant="outlined"
                    label="Fathers Name"
                    placeholder="Enter your father name"
                    onChange={(e) =>
                      setFormData({ ...formData, fathersName: e.target.value })
                    }
                  />
                </Grid>
                <Grid item xs={12} md={6}>
                  <TextField
                    required
                    fullWidth
                    variant="outlined"
                    label="Mothers Name"
                    placeholder="Enter your mother name"
                    onChange={(e) =>
                      setFormData({ ...formData, mothersName: e.target.value })
                    }
                  />
                </Grid>
                <Grid item xs={12} md={6}>
                  <TextField
                    disabled
                    label="Session"
                    defaultValue="2017-2018"
                    variant="outlined"
                    required
                    fullWidth
                  />
                </Grid>
                <Grid item xs={12} md={6}>
                  <TextField
                    label="Semester"
                    defaultValue="1"
                    variant="outlined"
                    fullWidth
                    disabled
                    required
                  />
                </Grid>
                <Grid item xs={12} md={12}>
                  <Typography variant="h6" align="center">
                    Date of Birth
                  </Typography>
                  <TextField
                    variant="outlined"
                    fullWidth
                    type="date"
                    required
                    onChange={(e) =>
                      setFormData({ ...formData, birthDate: e.target.value })
                    }
                  />
                </Grid>
              </>
            )}
            <Grid item xs={12}>
              <Button
                type="submit"
                variant="contained"
                color="primary"
                fullWidth
              >
                Register
              </Button>
            </Grid>
          </Grid>
        </Paper>
      </form>
    </Container>
  );
}
