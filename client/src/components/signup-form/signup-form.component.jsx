import React, { useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import {
  Paper,
  Button,
  TextField,
  Typography,
  FormControlLabel,
  Select,
  MenuItem
} from "@material-ui/core";

import Radio from "@material-ui/core/Radio";
import RadioGroup from "@material-ui/core/RadioGroup";
import FormControl from "@material-ui/core/FormControl";
import FormLabel from "@material-ui/core/FormLabel";

const useStyles = makeStyles((theme) => ({
  //   margin: {
  //     margin: theme.spacing(2),
  //   },
  //   paper: {
  //     padding: "30px",
  //     backgroundColor: "ecf0f4",
  //     borderRadius: "8px",
  //   },
  //   error: {
  //     color: "red",
  //   },

  root: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
    padding: theme.spacing(2),
    "& .MuiTextField-root": {
      margin: theme.spacing(1),
      width: "300px",
    },
    "& .MuiButtonBase-root": {
      margin: theme.spacing(2),
    },
  },
}));

const SignUpForm = () => {
  const classes = useStyles();

  const [department, setDepartment] = useState(0)

  const updateDepartMentValue = (e) => {
    setDepartment(e.target.value);
  }

  return (
    <Paper className={classes.paper} style={{ boxShadow: "none" }}>
      <h1 style={{ textAlign: "center" }}>SIGN UP</h1>
      
      <form className={classes.root}>
        <TextField label="First Name" variant="filled" required />
        <TextField label="Last Name" variant="filled" required />
        <Select value={department} onChange={updateDepartMentValue} variant="filled">
          <MenuItem value={0} disabled>Select Department</MenuItem>
          <MenuItem value={1}>ICE</MenuItem>
          <MenuItem value={2}>EEE</MenuItem>
          <MenuItem value={3}>ACCE</MenuItem>
        </Select>
        <FormControl component="fieldset" variant="filled">
          <RadioGroup
            aria-label="userrole"
            name="user1"
            row={true}
          >
            <FormControlLabel
              value="Teacher"
              control={<Radio />}
              label="Teacher"
            />
            <FormControlLabel value="Student" control={<Radio />} label="Student" />
          </RadioGroup>
        </FormControl>
        <TextField label="Email" variant="filled" type="email" required />
        <TextField label="Password" variant="filled" type="password" required />
        <Typography variant="h9">Upload your Id card</Typography>
        <TextField variant="filled" type="file" required />
        <div>
  <Button variant="contained">
    Cancel
  </Button>
  <Button type="submit" variant="contained" color="primary">
    Signup
  </Button>
</div>
      </form>
    </Paper>
  );
};

export default SignUpForm;
