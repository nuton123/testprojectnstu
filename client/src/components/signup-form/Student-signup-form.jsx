import React, { useState } from "react";
import {
  Grid,
  Paper,
  Avatar,
  Typography,
  TextField,
  Button,
  Select,
  MenuItem,
} from "@material-ui/core";
import { makeStyles } from "@material-ui/core";
import AddCircleOutlineOutlinedIcon from "@material-ui/icons/AddCircleOutlineOutlined";

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
    padding: theme.spacing(2),
    "& .MuiTextField-root": {
      margin: theme.spacing(1),
      width: "300px",
    },
    "& .MuiButtonBase-root": {
      margin: theme.spacing(2),
    },
  },
}));

const StudentSignUpForm = () => {
  const classes = useStyles();

  const paperStyle = { padding: 20, width: 300, margin: "0 auto" };
  const headerStyle = { margin: 0 };
  const avatarStyle = { backgroundColor: "#1bbd7e" };

  const [department, setDepartment] = useState(0);
  const [session, setSession] = useState(0);

  const updateDepartMentValue = (e) => {
    setDepartment(e.target.value);
  };

  const upDateSessionValue = (e) => {
    setSession(e.target.value);
  };

  return (
    <Grid>
      <Paper style={paperStyle}>
        <Grid align="center">
          <Avatar style={avatarStyle}>
            <AddCircleOutlineOutlinedIcon />
          </Avatar>
          <h2 style={headerStyle}>Sign Up</h2>
          <Typography variant="caption" gutterBottom>
            Please fill this form to create an account !
          </Typography>
        </Grid>
        <form className={classes.root}>
          <TextField fullWidth label="Name" placeholder="Enter your name" />
          <TextField fullWidth label="Email" placeholder="Enter your email" />
          <TextField
            fullWidth
            label="Password"
            placeholder="Enter your password"
            type="password"
          />
          <TextField
            fullWidth
            label="Confirm Password"
            placeholder="Confirm your password"
            type="password"
          />
          <Select fullWidth value={department} onChange={updateDepartMentValue}>
            <MenuItem value={0} disabled>
              Select Department
            </MenuItem>
            <MenuItem value={1}>ICE</MenuItem>
            <MenuItem value={2}>EEE</MenuItem>
            <MenuItem value={3}>ACCE</MenuItem>
          </Select>

          <Select fullWidth value={session} onChange={upDateSessionValue}>
            <MenuItem value={0} disabled>
              Select Sesion
            </MenuItem>
            <MenuItem value={1}>2014-2015</MenuItem>
            <MenuItem value={2}>2015-2016</MenuItem>
            <MenuItem value={3}>2016-2017</MenuItem>
            <MenuItem value={4}>2017-2018</MenuItem>
          </Select>
          <TextField
            fullWidth
            label="Your Department Batch Number"
            placeholder="Enter your batch number"
          />
          <TextField
            fullWidth
            label="Id Roll"
            placeholder="Enter your id number"
          />
          <Typography variant="caption" gutterBottom>
            Upload your Id card
          </Typography>
          <TextField fullWidth type="file" required />
          <Button type="submit" variant="contained" color="primary" fullWidth>
            Sign up
          </Button>
        </form>
      </Paper>
    </Grid>
  );
};

export default StudentSignUpForm;
