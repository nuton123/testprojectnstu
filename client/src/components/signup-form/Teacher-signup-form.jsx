import React, { useState } from "react";

import {
  Grid,
  Paper,
  Avatar,
  TextField,
  Button,
  Typography,
  Select,
  MenuItem,
} from "@material-ui/core";
import LockOutlinedIcon from "@material-ui/icons/LockOutlined";

const TeacherSignUpForm = ({ handleChange }) => {
  const paperStyle = {
    padding: 20,
    width: 300,
    margin: "0 auto",
  };
  const avatarStyle = { backgroundColor: "#1bbd7e" };
  const [department, setDepartment] = useState(0);

  const updateDepartMentValue = (e) => {
    setDepartment(e.target.value);
  };

  return (
    <Grid>
      <Paper style={paperStyle}>
        <Grid align="center">
          <Avatar style={avatarStyle}>
            <LockOutlinedIcon />
          </Avatar>
          <h2>Sign Up</h2>
          <Typography variant="caption" gutterBottom>
            Please fill this form to create an account !
          </Typography>
        </Grid>
        <form>
          <TextField fullWidth label="Name" placeholder="Enter your name" />
          <TextField fullWidth label="Email" placeholder="Enter your email" />
          <TextField
            fullWidth
            label="Password"
            placeholder="Enter your password"
            type="password"
          />
          <TextField
            fullWidth
            label="Confirm Password"
            placeholder="Confirm your password"
            type="password"
          />
          <Select fullWidth value={department} onChange={updateDepartMentValue}>
            <MenuItem value={0} disabled>
              Select Department
            </MenuItem>
            <MenuItem value={1}>ICE</MenuItem>
            <MenuItem value={2}>EEE</MenuItem>
            <MenuItem value={3}>ACCE</MenuItem>
          </Select>

          <TextField
            fullWidth
            label="Id Roll"
            placeholder="Enter your id number"
          />
          <Typography variant="caption" gutterBottom>
            Upload your Id card
          </Typography>
          <TextField fullWidth type="file" required />
          
          <Button type="submit" variant="contained" color="primary" fullWidth>
            Sign up
          </Button>
        </form>
      </Paper>
    </Grid>
  );
};

export default TeacherSignUpForm;
