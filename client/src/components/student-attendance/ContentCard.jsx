import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import { Link } from 'react-router-dom';
const useStyles = makeStyles({
  root: {
    minWidth: 275,
    margin: '10px',
  },
  bullet: {
    display: 'inline-block',
    margin: '0 2px',
    transform: 'scale(0.8)',
  },
  title: {
    fontSize: 14,
  },
  pos: {
    marginBottom: 12,
  },
  cardAction: {
      margin: '0 auto',
  }
});

export default function ContentCard(props) {
  const classes = useStyles();
  const {session, semister, description, percantage} = props;

  return (
    <Card className={classes.root}>
      <CardContent>
        <Typography className={classes.title} color="textSecondary" gutterBottom>
          session {session} 
        </Typography>
        <Typography variant="h5" component="h2">
          {semister}
        </Typography>
        <Typography variant="body2" component="p">
          {description}
          <br />
          {percantage}
        </Typography>
      </CardContent>
      <CardActions>
        <Button component={Link} to="/student-account/student/my-attendance/semister-1" size="small" className={classes.cardAction} color="primary" variant='contained'>Show Details</Button>
      </CardActions>
    </Card>
  );
}
