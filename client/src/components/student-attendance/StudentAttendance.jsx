import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import data from "./semisterData";
import ContentCard from "./ContentCard";
import { Container } from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  container: {
    display: "flex",
    flexDirection: "column",
  },
  paper: {
    padding: theme.spacing(1),
    textAlign: "center",
    color: theme.palette.text.secondary,
    whiteSpace: "nowrap",
    marginBottom: theme.spacing(1),
  },
  divider: {
    margin: theme.spacing(2, 0),
  },
}));

const StudentAttendance = () => {
  const classes = useStyles();

  return (
    <Container className={classes.container}>
      <Grid container spaccing={2}>
        {data.map((a, index) => (
          <Grid item key={index}>
            <ContentCard
              session={a.session}
              semister={a.semister}
              description={a.description}
              percantage={a.percantage}
            />
          </Grid>
        ))}
      </Grid>
    </Container>
  );
};

export default StudentAttendance;
