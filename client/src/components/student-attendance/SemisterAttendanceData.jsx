import React from "react";
import { withStyles, makeStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import { Container, Grid } from "@material-ui/core";

const StyledTableCell = withStyles((theme) => ({
  head: {
    backgroundColor: theme.palette.common.black,
    color: theme.palette.common.white,
  },
  body: {
    fontSize: 14,
  },
}))(TableCell);

const StyledTableRow = withStyles((theme) => ({
  root: {
    "&:nth-of-type(odd)": {
      backgroundColor: theme.palette.action.hover,
    },
  },
}))(TableRow);

function ccyFormat(num) {
  return `${num.toFixed(2)}`;
}


function attendRow(totalClasses, present) {
  return (present * 100) / totalClasses;
}
function createData(courses, totalClasses, present, absent) {
  const attend = attendRow(totalClasses, present);
  return { courses, totalClasses, present, absent, attend };
}

function subtotal(items) {
  return items.map(({ attend }) => attend).reduce((sum, i) => (sum + i)/100, 0);
}
const rows = [
  createData("ICE-4201", 40, 20, 20),
  createData("ICE-4203", 36, 30, 6),
  createData("ICE-4205", 42, 16.0, 26),
  createData("ICE-4206", 33, 30, 3),
  createData("ICE-4207", 25, 19, 6),
];

const useStyles = makeStyles({
  table: {
    minWidth: 700,
  },
  centerGrid: {
    width: "100%",
    margin: "0 auto",
  },
});

const totalAttendance = subtotal(rows);

export default function SemisterAttendanceData() {
  const classes = useStyles();

  return (
    <Container>
      <Grid
        item
        xs={9}
        container
        justify="center"
        alignItems="center"
        direction="column"
        className={classes.centerGrid}
      >
        <TableContainer component={Paper}>
          <Table className={classes.table} aria-label="customized table">
            <TableHead>
              <TableRow>
                <StyledTableCell>Course Code</StyledTableCell>
                <StyledTableCell align="right">Total Classes</StyledTableCell>
                <StyledTableCell align="right">Present</StyledTableCell>
                <StyledTableCell align="right">Absent</StyledTableCell>
                <StyledTableCell align="right">Attendance(%)</StyledTableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {rows.map((row) => (
                <StyledTableRow key={row.courses}>
                  <StyledTableCell component="th" scope="row">
                    {row.courses}
                  </StyledTableCell>
                  <StyledTableCell align="right">
                    {row.totalClasses}
                  </StyledTableCell>
                  <StyledTableCell align="right">{row.present}</StyledTableCell>
                  <StyledTableCell align="right">{row.absent}</StyledTableCell>
                  <StyledTableCell align="right">{ccyFormat(row.attend)}</StyledTableCell>
                </StyledTableRow>
              ))}

              <TableRow>
                <TableCell rowSpan={3} />
                <TableCell colSpan={2}>Total Average Attendance</TableCell>
                <TableCell align="right">
                  {ccyFormat(totalAttendance)}%
                </TableCell>
              </TableRow>
            </TableBody>
          </Table>
        </TableContainer>
      </Grid>
    </Container>
  );
}
