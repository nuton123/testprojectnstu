import React from "react";
import PropTypes from "prop-types";
import { makeStyles } from "@material-ui/core/styles";
import Box from "@material-ui/core/Box";
import Collapse from "@material-ui/core/Collapse";
import Container from "@material-ui/core/Container";
import Grid from "@material-ui/core/Grid";
import IconButton from "@material-ui/core/IconButton";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Typography from "@material-ui/core/Typography";
import Paper from "@material-ui/core/Paper";
import KeyboardArrowDownIcon from "@material-ui/icons/KeyboardArrowDown";
import KeyboardArrowUpIcon from "@material-ui/icons/KeyboardArrowUp";

const useRowStyles = makeStyles({
  root: {
    "& > *": {
      borderBottom: "unset",
    },
  },
});

function createData(courses, totalClasses, present, absent, average) {
  return {
    courses,
    totalClasses,
    present,
    absent,
    average,  
    history: [
      { date: "2020-01-05", status: "P"},
      { date: "2020-01-02", status: "P"},
      { date: "2020-01-03", status: "A"},
      { date: "2020-01-07", status: "P"},
    ],
  };
}

function Row(props) {
  const { row } = props;
  const [open, setOpen] = React.useState(false);
  const classes = useRowStyles();

  return (
    <React.Fragment>
      <TableRow className={classes.root}>
        <TableCell>
          <IconButton
            aria-label="expand row"
            size="small"
            onClick={() => setOpen(!open)}
          >
            {open ? <KeyboardArrowUpIcon /> : <KeyboardArrowDownIcon />}
          </IconButton>
        </TableCell>
        <TableCell component="th" scope="row">
          {row.courses}
        </TableCell>
        <TableCell align="right">{row.totalClasses}</TableCell>
        <TableCell align="right">{row.present}</TableCell>
        <TableCell align="right">{row.absent}</TableCell>
        <TableCell align="right">{row.average}</TableCell>
      </TableRow>
      <TableRow>
        <TableCell style={{ paddingBottom: 0, paddingTop: 0 }} colSpan={6}>
          <Collapse in={open} timeout="auto" unmountOnExit>
            <Box margin={1}>
              <Typography variant="h6" gutterBottom component="div">
                Attendance History
              </Typography>
              <Table size="small" aria-label="purchases">
                <TableHead>
                  <TableRow>
                    <TableCell>Date</TableCell>
                    <TableCell>Status</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {row.history.map((historyRow) => (
                    <TableRow key={historyRow.date}>
                      <TableCell component="th" scope="row">
                        {historyRow.date}
                      </TableCell>
                      <TableCell>{historyRow.status}</TableCell>
                    </TableRow>
                  ))}
                </TableBody>
              </Table>
            </Box>
          </Collapse>
        </TableCell>
      </TableRow>
    </React.Fragment>
  );
}

Row.propTypes = {
  row: PropTypes.shape({
    totalClasses: PropTypes.number.isRequired,
    absent: PropTypes.number.isRequired,
    present: PropTypes.number.isRequired,
    history: PropTypes.arrayOf(
      PropTypes.shape({
        amount: PropTypes.number.isRequired,
        customerId: PropTypes.string.isRequired,
        date: PropTypes.string.isRequired,
      })
    ).isRequired,
    name: PropTypes.string.isRequired,
    average: PropTypes.number.isRequired,
  }).isRequired,
};

const rows = [
  createData("ICE-4201", 55, 30, 15, 4.0),
  createData("ICE-4202", 45, 40, 5, 4.3),
  createData("ICE-4206", 36, 25, 9, 6.0),
  createData("ICE-4102", 33, 22, 10, 4.3),
  createData("ICE-1202", 25, 24, 1, 3.9),
];

const useStyles = makeStyles({
  centerGrid: {
    width: "100%",
    margin: "0 auto",
  },
});

export default function AllClassesAttendance() {
  const classes = useStyles();

  return (
    <Container>
      <Grid
        item
        xs={9}
        container
        justify="center"
        alignItems="center"
        direction="column"
        className={classes.centerGrid}
      >
          
        <TableContainer component={Paper}>
          <Table aria-label="collapsible table">
            <TableHead>
              <TableRow>
                <TableCell />
                <TableCell>Courses</TableCell>
                <TableCell align="right">Total Classes</TableCell>
                <TableCell align="right">Present</TableCell>
                <TableCell align="right">Absent</TableCell>
                <TableCell align="right">Average&nbsp;(%)</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {rows.map((row) => (
                <Row key={row.courses} row={row} />
              ))}
            </TableBody>
          </Table>
        </TableContainer>
      </Grid>
    </Container>
  );
}
