import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import {Container, Paper, Grid, Typography} from '@material-ui/core';

import Logo from '../../assets/nstu-logo.png';

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing(2),
    margin: theme.spacing(2, 0),
    textAlign: 'center',
    boxShadow: '0 0 28px rgb(0 0 0 / 8%)'
  },
}));

export default function CenteredGrid() {
  const classes = useStyles();

  return (
    <Container>
      <Grid container spacing={0} alignItems='center'>
        <Grid item xs={12}>
          <Paper className={classes.paper} style={{borderRadius: '10px'}}>
            <img src={Logo} alt="" style={{height: 'auto', width: '80px'}}/>
            <Typography variant='h4' style={{color: '#202342', fontWeight: '700', fontSize: '1.75rem'}}>
              Noakhali Science & Technology University
            </Typography>
          </Paper>
        </Grid>
      </Grid>
    </Container>
  );
}
