import React from "react";
import {
  Container,
  Grid,
  Paper,
  TextField,
  Typography,
  Card,
  Button,
} from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing(1),
    textAlign: "center",
    color: theme.palette.text.secondary,
    margin: "20px",
    height: "250px",
  },
  outerPaper: {
    margin: "20px 0px",
    background: "#F1F2F3",
  },
  cardSpacing: {
    display: "flex",
    width: "100%",
    margin: "10px 20px",
    padding: "10px",
  },
  flexGrid: {
    display: "flex",
  },
  innerflexGrid: {
    display: "flex",
    marginRight: "30px",
  },
}));

const VersityDashboard = () => {
  const classes = useStyles();

  return (
    <Container>
      <Paper className={classes.outerPaper}>
        <Grid container spacing={2} columnSpacing={{ xs: 1, sm: 2, md: 3 }}>
          <Grid item xs={4}>
            <Paper className={classes.paper}>
              <Typography>Total Teachers</Typography>
              <Typography>56</Typography>
            </Paper>
          </Grid>
          <Grid item xs={4}>
            <Paper className={classes.paper}>
              <Typography>Total Students</Typography>
              <Typography>1300</Typography>
            </Paper>
          </Grid>
          <Grid item xs={4}>
            <Paper className={classes.paper}>
              <Typography>Total Deaprtments</Typography>
              <Typography>30</Typography>
            </Paper>
          </Grid>
        </Grid>
      </Paper>
      <Paper className={classes.outerPaper}>
        <Grid container>
          <Grid item xs={12}>
            <Typography>Total Active Mangement Account(3 left 0f 3)</Typography>
          </Grid>
        </Grid>
        <Grid container spacing={2} columnSpacing={{ xs: 1, sm: 2, md: 3 }}>
          <Card className={classes.cardSpacing}>
            <Grid item xs={8} className={classes.flexGrid}>
              <Grid className={classes.innerflexGrid}>
                <Typography>Name:</Typography>
                <Typography>Nuton Chakma</Typography>
              </Grid>
              <Grid className={classes.innerflexGrid}>
                <Typography>Eamil:</Typography>
                <Typography>nuton121@gmail.com</Typography>
              </Grid>
            </Grid>
            <Grid item x={4} spacing={2}>
              <Button variant="contained" color="primary" size="small">
                Active
              </Button>
              <Button variant="contained" color="secondary" size="small">
                Delete
              </Button>
            </Grid>
          </Card>
          <Card className={classes.cardSpacing}>
            <Grid item xs={8} className={classes.flexGrid}>
              <Grid className={classes.innerflexGrid}>
                <Typography>Name:</Typography>
                <Typography>Nuton Chakma</Typography>
              </Grid>
              <Grid className={classes.innerflexGrid}>
                <Typography>Eamil:</Typography>
                <Typography>nuton121@gmail.com</Typography>
              </Grid>
            </Grid>
            <Grid item x={4} spacing={2}>
              <Button variant="contained" color="primary" size="small">
                Active
              </Button>
              <Button variant="contained" color="secondary" size="small">
                Delete
              </Button>
            </Grid>
          </Card>
          <Card className={classes.cardSpacing}>
            <Grid item xs={8} className={classes.flexGrid}>
              <Grid className={classes.innerflexGrid}>
                <Typography>Name:</Typography>
                <Typography>Nuton Chakma</Typography>
              </Grid>
              <Grid className={classes.innerflexGrid}>
                <Typography>Eamil:</Typography>
                <Typography>nuton121@gmail.com</Typography>
              </Grid>
            </Grid>
            <Grid item x={4} spacing={2}>
              <Button variant="contained" color="primary" size="small">
                Active
              </Button>
              <Button variant="contained" color="secondary" size="small">
                Delete
              </Button>
            </Grid>
          </Card>
        </Grid>
      </Paper>
    </Container>
  );
};

export default VersityDashboard;
