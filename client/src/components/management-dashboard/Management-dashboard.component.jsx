import React, { useState } from "react";
import {
  Container,
  Grid,
  Button,
  Select,
  MenuItem,
  Card,
  CardContent,
  Typography,
  TextField,
} from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  Card: {
    margin: "20px 0",
  },
  paper: {
    padding: theme.spacing(1),
    textAlign: "center",
    color: theme.palette.text.secondary,
    display: "flex",
    alignItems: "center",
    margin: "0 auto",
  },

  container: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
  },
  spacingItem: {
    margin: "20px",
  },
  flexItem: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    margin: "5px 0px",
  },
  flexButton: {
    margin: "0px 10px",
  },
  bottomItem: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    margin: "20px 0px",
  },
}));

const ManagementDashboard = () => {
  const classes = useStyles();
  const [formData, setFormData] = useState({
    session: "",
    department: "",
  });

  const [tokenType, setTokenType] = useState("");

  const updateTokenTypeValue = (e) => {
    setTokenType(e.target.value);
  };

  const handleOnSubmit = (e) => {
    e.preventDefault();
    return alert("success");
  };

  return (
    <Container>
      <Card className={classes.Card}>
        <CardContent>
          <form onSubmit={handleOnSubmit}>
            <Grid container spacing={4} className={classes.container}>
              <Grid className={classes.spacingItem}>
                <Select value={tokenType} onChange={updateTokenTypeValue}>
                  <MenuItem value="0" disabled>
                    Select Token Type
                  </MenuItem>
                  <MenuItem value="1">Student</MenuItem>
                  <MenuItem value="2">Teacher</MenuItem>
                </Select>
              </Grid>
              <Grid>
                {tokenType && tokenType === "1" && (
                  <Grid>
                    <Grid className={classes.spacingItem}>
                      <Select
                        displayEmpty
                        onChange={(e) =>
                          setFormData({ ...formData, session: e.target.value })
                        }
                      >
                        <MenuItem disabled>Select Session</MenuItem>
                        <MenuItem value="1">2016-2017</MenuItem>
                        <MenuItem value="2">2017-2018</MenuItem>
                        <MenuItem value="3">2018-2019</MenuItem>
                      </Select>
                    </Grid>
                  </Grid>
                )}
              </Grid>

              <Grid className={classes.spacingItem}>
                <Select
                  displayEmpty
                  onChange={(e) =>
                    setFormData({ ...formData, department: e.target.value })
                  }
                >
                  <MenuItem disabled>Select Department</MenuItem>
                  <MenuItem value="1">ICE</MenuItem>
                  <MenuItem value="2">EEE</MenuItem>
                  <MenuItem value="3">ACCE</MenuItem>
                </Select>
              </Grid>

              <Grid>
                <Button
                  type="submit"
                  variant="contained"
                  color="primary"
                  className={classes.spacingItem}
                >
                  Generate Token
                </Button>
              </Grid>
            </Grid>
          </form>

          <Grid item xs={12} className={classes.bottomItem}>
            <TextField variant="outlined" type="texfield" />
            <Button size="small" color="primary" variant="contained">
              Copy
            </Button>
          </Grid>
        </CardContent>
      </Card>

      <Card>
        <CardContent>
          <Grid
            container
            rowSpacing={1}
            columnSpacing={{ xs: 1, sm: 2, md: 3 }}
          >
            <Grid item xs={12}>
              <Typography variant="subtitle1">
                Recent Generated Token
              </Typography>
            </Grid>

            <Grid item xs={12} className={classes.flexItem}>
              <Typography>LUCEJHYV</Typography>
              <Button
                size="small"
                variant="contained"
                color=""
                className={classes.flexButton}
              >
                Copy
              </Button>
              <Button
                size="small"
                variant="contained"
                color="primary"
                className={classes.flexButton}
              >
                Active
              </Button>
            </Grid>

            <Grid item xs={12} className={classes.flexItem} spacing={2}>
              <Typography>LUCEJHYV</Typography>
              <Button
                size="small"
                variant="contained"
                color=""
                className={classes.flexButton}
              >
                Copy
              </Button>
              <Button
                size="small"
                variant="contained"
                color="secondary"
                className={classes.flexButton}
              >
                Expired
              </Button>
            </Grid>
          </Grid>
        </CardContent>
      </Card>
    </Container>
  );
};

export default ManagementDashboard;
