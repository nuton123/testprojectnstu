import React, {useState} from 'react';
import MaterialTable from "@material-table/core";
import { Link } from "react-router-dom";
import { makeStyles, Button, Container } from '@material-ui/core';
import VisibilityOutlinedIcon from '@material-ui/icons/VisibilityOutlined';

import { tableIcons } from '../../lib/material-table-config';

const useStyles = makeStyles((theme) => ({
  tableStyle: {
    boxShadow: '0 0 28px rgb(0 0 0 / 8%)',
  },

  button: {
    width: '100px',
    height: '40px',
    borderRadius: '50px',
    textTransform: 'uppercase',
    fontWeight: '800',
    background: '#3498db',
  }
}));

export default function StudentDetailsTable() {
  const classes = useStyles();
  const [selectedRow, setSelectedRow] = useState(null);
  
  return (
    <Container>
      <MaterialTable
        className={classes.tableStyle}
        style={{borderRadius: '10px'}}
        title="Students Records for Course Teacher"
        icons={tableIcons}
        columns={[
          { title: 'Student Photo', field: 'photo', render: rowData => <img src={rowData.photo} alt='' style={{width: '50%'}} /> },
          { title: 'Student ID', field: 'studentId' },
          { title: 'Student Name', field: 'studentName' },
          { title: 'Session', field: 'session' },
          { title: 'Hall Name', field: 'hallName' },
          { title: 'All Details', field: 'button',
              render: (rowData) => rowData && (
                <Button 
                component={Link}
        to="/teacher-account/teacher/co-ordinator/ice/batch5/student-id-1"
                  variant="contained"
                  color="primary"
                  size="small"
                  className={classes.button}
                  startIcon={<VisibilityOutlinedIcon />}
                >
                  View
                </Button>
              )},
        ]}
        data={[
          { studentId: '2050608001', studentName: 'Student 1', session: '2016-2017', hallName: 'ASH', photo: 'https://www.w3schools.com/w3images/avatar2.png' },
          { studentId: '2050608002', studentName: 'Student 2', session: '2016-2017', hallName: 'ASH', photo: 'https://www.pngkey.com/png/full/114-1149878_setting-user-avatar-in-specific-size-without-breaking.png' },
        ]}
        onRowClick={((evt, selectedRow) => setSelectedRow(selectedRow.tableData.id))}
        options={{
          rowStyle: rowData => ({
            backgroundColor: (selectedRow === rowData.tableData.id) ? '#EEE' : '#FFF'
          })
        }}
      />
    </Container>
  )
}
