import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import {Grid, Typography} from '@material-ui/core';

// import {createStructuredSelector} from 'reselect';
// import { connect } from 'react-redux';
// import { selectCurrentUser } from '../../redux/user/user.selectors';

import MenuDropdown from '../menu-dropdown/Menu-dropdown.component';
import Profile from '../../assets/profile.jpg';

const useStyles = makeStyles(() => ({
  image: {
    height: '100%',
    width: '100%',
    borderRadius: '50%',
    boxShadow: '0 0 28px rgb(0 0 0 / 8%)',
  }
}));

function Navbar() {
  const classes = useStyles();
 
  return (
    <Grid container>
        <Grid item xs={12} style={{height: '70px', width: '100%', background: '#fff', display: 'flex', justifyContent: 'flex-end'}}>
            <div style={{display: 'flex', alignItems: 'center'}}>
                <div style={{height: '52px', width: '52px', marginRight: '10px'}}>
                    <img src={Profile} alt={'Profile'} className={classes.image} />
                </div>
                <Typography variant='subtitle1'>
                    Mr Teacher
                </Typography>
                <MenuDropdown />
            </div>
        </Grid>
    </Grid>
  );
}

export default Navbar;

// const mapStateToProps = createStructuredSelector(
//   {
//       userName: selectCurrentUser
//   }
// );

// export default connect(mapStateToProps, null)(Navbar);
