import React from "react";
import { Link } from "react-router-dom";
import { Typography } from "@material-ui/core";
import { makeStyles } from "@material-ui/core";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import IconButton from "@material-ui/core/IconButton";
import DeleteIcon from "@material-ui/icons/Delete";
import CardActionArea from "@material-ui/core/CardActionArea";

const useStyles = makeStyles(() => ({
  root: {},
  centerGrid: {
    width: "100%",
    margin: "0 auto",
  },
  paper1: {
    minWidth: "500px",
    margin: "50px auto",
  },
  headingTop: {
    display: "flex",
    justifyContent: "center",
    paddingTop: "50px",
  },
  batchCard: {
    minWidth: 275,
    margin: "10px",
  },
}));

const ContentCard = (props) => {
  const classes = useStyles();
  const { departmentName,session, batch, courseStartDate } = props;

  return (
    <Card className={classes.batchCard} elevation={3}>
      <CardActionArea
        component={Link}
        to="/teacher-account/teacher/co-ordinator/ice/batch5"
      >
        <CardContent>
          <Typography
            className={classes.title}
            color="textSecondary"
            gutterBottom
          >
            Department: {departmentName}
          </Typography>
          <Typography variant="body2" component="p">
            Session: {session}
          </Typography>
          <Typography variant="body2" component="p">
            Batch: {batch}
          </Typography>
          <Typography variant="body2" component="p">
            Start Date: {courseStartDate}
          </Typography>
        </CardContent>
      </CardActionArea>

      <IconButton aria-label="delete">
        <DeleteIcon />
      </IconButton>
    </Card>
  );
};

export default ContentCard;
