export const batchData =[
    {
        departmentName: 'ICE',
        session:'2016-2017',
        batch:'5th',
        courseStartDate:'4/14/2022'
    },
    {
        departmentName: 'ICE',
        session:'2016-2017',
        batch:'6th',
        courseStartDate:'4/14/2022'
    },
    {
        departmentName: 'ICE',
        session:'2016-2017',
        batch:'7th',
        courseStartDate:'4/14/2022'
    },
    {
        departmentName: 'ICE',
        session:'2016-2017',
        batch:'8th',
        courseStartDate:'4/14/2022'
    },
    
    
]

export default batchData;