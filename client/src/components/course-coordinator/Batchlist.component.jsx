import React from "react";
import { Container, Grid } from "@material-ui/core";
import { makeStyles } from "@material-ui/core";
import ContentCard from './ContentCard';
import batchData from "./batchData";


const useStyles = makeStyles(() => ({
  root: {},
  centerGrid: {
    width: "100%",
    margin: "0 auto",
  },
  paper1: {
    minWidth: "500px",
    margin: "50px auto",
  },
  headingTop: {
    display: "flex",
    justifyContent: "center",
    paddingTop: "50px",
  },
  courseCard: {
    maxWidth: 275,
    marginTop: "30px",
  },
}));

const BatchList = () => {
  const classes = useStyles();

  /**  const [courses, setCourses] = useState([]);

  useEffect(() => {
    loadCoures();
  }, []);

  const loadCoures = () => {
    const result = axios.get("http://localhost:3003/courses");
    setCourses(result.data.reverse());
  };
 */
  
  return (
    <Container className={classes.container}>
        <Grid container>
        {batchData.map((a, index) => (
          <Grid item key={index}>
            <ContentCard
              departmentName={a.departmentName}
              session={a.session}
              batch={a.batch}
              courseStartDate={a.courseStartDate}

            />
          </Grid>
        ))}
        </Grid>
    </Container>
  );
};

export default BatchList;
