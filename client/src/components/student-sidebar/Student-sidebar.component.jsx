import React from 'react';
import { makeStyles, MenuList, MenuItem, Typography } from '@material-ui/core';
import { useHistory, useLocation,useRouteMatch   } from 'react-router-dom';
import { AccountCircleOutlined, 
    ImportContactsOutlined, 
   
} from '@material-ui/icons';

// import { connect } from 'react-redux';
// import { createStructuredSelector } from 'reselect';
// import { selectCurrentUser } from '../../redux/user/user.selectors';

const useStyles = makeStyles(() => ({
    menuItemRoot: {
        "&$menuItemSelected, &$menuItemSelected:focus, &$menuItemSelected:hover": {
            backgroundColor: "#2D46B9",
            margin: '0 8px',
            borderRadius: '4px'
        },
        color: '#fff',
        padding: '15px',
        fontSize: '16px',
        fontWeight: '300',
        letterSpacing: '0.03em',
    },
    menuItemSelected: {},
    heading: {
        color: '#fff',
        fontSize: '1.5rem',
        fontWeight: '700',
        padding: '10px 0 20px 0',
        textAlign: 'center',
    }
}));


function StudentSidebar() {
    const classes = useStyles();
    let match = useRouteMatch();
    let location = useLocation();
    let history = useHistory();
  
    return (
        <MenuList>
            <Typography variant='h4' className={classes.heading}>
                Menu
            </Typography>
            <MenuItem
             classes={{ 
                root: classes.menuItemRoot,
                selected: classes.menuItemSelected
             }}
             onClick={ () =>  history.push(`${match.url}/dashboard`) }
             selected={location.pathname === `${match.url}/dashboard`}   
            ><AccountCircleOutlined />&nbsp; My Dashboard</MenuItem>
            <MenuItem
             classes={{ 
                root: classes.menuItemRoot,
                selected: classes.menuItemSelected
             }}
             onClick={ () =>  history.push(`${match.url}/student/profile`) }
             selected={location.pathname === `${match.url}/student/profile`}   
            ><AccountCircleOutlined />&nbsp; My Profile</MenuItem>
            <MenuItem
             classes={{ 
                root: classes.menuItemRoot,
                selected: classes.menuItemSelected
             }}
             onClick={ () => history.push(`${match.url}/student/my-attendance`) }
             selected={location.pathname === `${match.url}/student/my-attendance`}  
             ><ImportContactsOutlined />&nbsp; My Attendance</MenuItem>
             <MenuItem
             classes={{ 
                root: classes.menuItemRoot,
                selected: classes.menuItemSelected
             }}
             onClick={ () => history.push(`${match.url}/student/my-all-classes`) }
             selected={location.pathname === `${match.url}/student/my-all-classes`}  
             ><ImportContactsOutlined />&nbsp; My All Classes</MenuItem>
            
        </MenuList>
    );
};

export default StudentSidebar;