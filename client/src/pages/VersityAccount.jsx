import React from "react";
import { Grid } from "@material-ui/core";

// import Header from '../components/header/header.component';
import Footer from "../components/footer/footer.component";
import Navbar from "../components/navbar/navbar.component";
import VersitySidebar from "../components/versity-sidebar/Versity-sidebar.component";
import VersityAccountRoutes from "../routes/Versity-account-routes";

function VersityAccount() {
  return (
    <Grid container>
      <Grid
        item
        xs={2}
        style={{
          background: "#0b132b",
          height: "100vh",
          width: "100%",
          position: "fixed",
        }}
      >
        <VersitySidebar />
      </Grid>
      <Grid
        item
        xs={10}
        style={{ position: "absolute", right: "0", width: "100%" }}
      >
        <Navbar />
        {/* <Header /> */}
        <VersityAccountRoutes />
        <Footer />
      </Grid>
    </Grid>
  );
}

export default VersityAccount;
