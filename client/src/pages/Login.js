import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import {Container, Paper, Grid, Typography} from '@material-ui/core';

import SignInForm from '../components/signin-form/Signin-form.component';

import Logo from '../assets/nstu-logo.png';

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing(0),
    textAlign: 'center',
    boxShadow: '0 0 28px rgb(0 0 0 / 8%)',
  },
  container: {
    display: 'flex',
    height: '100vh',
    // margin: 'auto',
    justifyContent: 'center',
    alignItems: 'center',
  },
}));

export default function Signin() {
  const classes = useStyles();

  return (
    <Container className={classes.container}>
      <Grid container spacing={3} alignItems='center'>
        <Grid item xs={7}>
          <Paper className={classes.paper} style={{background: 'none', boxShadow: 'none'}} >
                <img src={Logo} alt={'Logo'} style={{height: '400px', width: 'auto'}}/>
                <Typography
                    variant='h3'
                    style={{fontWeight: '700', color: '#202342', fontSize: '2.5rem'}}
                >
                    Noakhali Science & Technology University
                </Typography>
          </Paper>
        </Grid>
        <Grid item xs={5}>
          <Paper className={classes.paper}>
              <SignInForm />
          </Paper>
        </Grid>
      </Grid>
    </Container>
  );
}