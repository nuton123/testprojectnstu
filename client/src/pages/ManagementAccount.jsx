import React from 'react';
import { Grid } from '@material-ui/core';

import Header from '../components/header/header.component';
import Footer from '../components/footer/footer.component';
import Navbar from '../components/navbar/navbar.component';
import ManagementAccountRoutes from '../routes/Management-account-routes';
import ManagementSidebar from '../components/management-sidebar/Management-sidebar';

function ManagementAccount() {


  return (
    <Grid container>
      <Grid item xs={2} style={{background: '#0b132b', height: '100vh', width: '100%', position: 'fixed'}}>
        <ManagementSidebar />
      </Grid>
      <Grid item xs={10} style={{position: 'absolute', right: '0', width: '100%'}}>
        <Navbar />
        <Header />
        <ManagementAccountRoutes />
        <Footer />
      </Grid>
    </Grid>
  );
};

export default ManagementAccount;
