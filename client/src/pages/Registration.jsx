import React from "react";
import { Grid, Container } from "@material-ui/core";
import { makeStyles } from "@material-ui/core";
import RegistrationForm from "../components/registration-from/Registration-form.component";

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
    padding: theme.spacing(2),
    "& .MuiTextField-root": {
      margin: theme.spacing(1),
      width: "300px",
    },
    "& .MuiButtonBase-root": {
      margin: theme.spacing(2),
    },
  },
  form: {
    width: "800px",
    margin: "0 auto",
    marginTop: "40px",
  },
}));

const Registration = () => {
  const classes = useStyles();

  return (
    <Container>
      <Grid container className={classes.form}>
        <RegistrationForm />
      </Grid>
    </Container>
  );
};

export default Registration;
