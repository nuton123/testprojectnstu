import React from 'react';
import { Grid } from '@material-ui/core';

import StudentSidebar from '../components/student-sidebar/Student-sidebar.component';
import Header from '../components/header/header.component';
import Footer from '../components/footer/footer.component';
import Navbar from '../components/navbar/navbar.component';
import StudentAccountRoutes from '../routes/Student-account-routes';

function StudentAccount() {


  return (
    <Grid container>
      <Grid item xs={2} style={{background: '#0b132b', height: '100vh', width: '100%', position: 'fixed'}}>
        <StudentSidebar />
      </Grid>
      <Grid item xs={10} style={{position: 'absolute', right: '0', width: '100%'}}>
        <Navbar />
        <Header />
        <StudentAccountRoutes />
        <Footer />
      </Grid>
    </Grid>
  );
};

export default StudentAccount;
