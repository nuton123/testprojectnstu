import React, { useState } from "react";
import { Grid, Paper, Avatar, TextField, Button } from "@material-ui/core";
import { makeStyles } from "@material-ui/core";
import AddCircleOutlineOutlinedIcon from "@material-ui/icons/AddCircleOutlineOutlined";

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
    padding: theme.spacing(2),
    "& .MuiTextField-root": {
      margin: theme.spacing(1),
      width: "300px",
    },
    "& .MuiButtonBase-root": {
      margin: theme.spacing(2),
    },
  },
}));

const RegisterWithTokenForm = ({ history }) => {
  const classes = useStyles();

  const [token, setToken] = useState('');
  const [isLoading, setIsLoading] = useState(false);
  const [isEroor, setIsError] = useState(false);
  const [errorMessage, setErrorMessage] = useState('');

  const paperStyle = { padding: 20, width: 300, margin: "0 auto" };
  const headerStyle = { margin: 0 };
  const avatarStyle = { backgroundColor: "#1bbd7e" };

  const handleSubmit = (e) => {
    e.preventDefault();
    //TODO: FETCH TOKEN data
    const fetchedData = {
      tokenType: 'teacher'
    }
    if(fetchedData.tokenType === 'teacher') {
      history.push('/register-form')
    }
  }

  return (
    <Grid container>
      <Paper style={paperStyle}>
        <Grid item align="center">
          <Avatar style={avatarStyle}>
            <AddCircleOutlineOutlinedIcon />
          </Avatar>
          <h2 style={headerStyle}>Token</h2>
        </Grid>
        <form className={classes.root} onSubmit={handleSubmit}>
          <TextField fullWidth label="Token" placeholder="Enter your token" value={token}
            onChange={(e) => setToken(e.target.value)}
          />
          <Button type="submit" variant="contained" color="primary" fullWidth>
            Submit
          </Button>
        </form>
        <Grid item align="center">
          <Button onClick={() => history.push('/')}>
            Already have an account? Login
          </Button>
        </Grid>
      </Paper>
    </Grid>
  );
};

export default RegisterWithTokenForm;
