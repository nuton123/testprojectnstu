import React from "react";
import { Grid } from "@material-ui/core";

import TeacherSidebar from "../components/teacher-sidebar/Teacher-sidebar.component";
import Header from "../components/header/header.component";
import Footer from "../components/footer/footer.component";
import Navbar from "../components/navbar/navbar.component";

import TeacherAccountRoutes from "../routes/Teacher-account-routes";

function TeacherAccount() {
  return (
    <Grid container>
      <Grid
        item
        xs={2}
        style={{
          background: "#0b132b",
          height: "100vh",
          width: "100%",
          position: "fixed",
        }}
      >
        <TeacherSidebar />
      </Grid>
      <Grid
        item
        xs={10}
        style={{ position: "absolute", right: "0", width: "100%" }}
      >
        <Navbar />
        {/* <Header /> */}
        <TeacherAccountRoutes />
        <Footer />
      </Grid>
    </Grid>
  );
}

export default TeacherAccount;
