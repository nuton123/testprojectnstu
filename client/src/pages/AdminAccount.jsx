import React from 'react';
import { Grid } from '@material-ui/core';

import AdminSidebar from '../components/admin-sidebar/Admin-sidebar.component';
import Header from '../components/header/header.component';
import Footer from '../components/footer/footer.component';
import Navbar from '../components/navbar/navbar.component';

import AdminAccountRoutes from '../routes/Admin-account-routes'

function AdminAccount() {


  return (
    <Grid container>
      <Grid item xs={2} style={{background: '#0b132b', height: '100vh', width: '100%', position: 'fixed'}}>
        <AdminSidebar />
      </Grid>
      <Grid item xs={10} style={{position: 'absolute', right: '0', width: '100%'}}>
        <Navbar />
        <Header />
        <AdminAccountRoutes />
        <Footer />
      </Grid>
    </Grid>
  );
};

export default AdminAccount;
