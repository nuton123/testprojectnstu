import React from 'react';
import {Container, Grid} from '@material-ui/core';

import StudentIdentity from '../../../components/student-identity/Student-identity.component'
import StudentDetails from '../../../components/student-details/Student-details.component';

function StudentProfileView() {
    
    return (
      <Container>
        <Grid container spacing={3}>
            <Grid item xs={4} >
                <StudentIdentity/>
            </Grid>
            <Grid item xs={8} >
                <StudentDetails />
            </Grid>
        </Grid>
      </Container>
    );
};

export default StudentProfileView;