import {
  Container,
  Grid
  
} from "@material-ui/core";
import React from "react";
import StudentDashboard from "../../../components/student-dashboard/Student-dashboard.component";

const StudentDashboardView = () => {
  return (
    <Container>
    <Grid container spacing={3}>
        <Grid item xs={12} >
            <StudentDashboard/>
        </Grid>
       
    </Grid>
  </Container>
  );
};

export default StudentDashboardView;
