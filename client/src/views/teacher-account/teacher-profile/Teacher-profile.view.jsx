import React from 'react';
import {Container, Grid} from '@material-ui/core';

// import {connect} from 'react-redux';
// import {createStructuredSelector} from 'reselect';
// import {selectCurrentUser} from '../../../redux/user/user.selectors';

import TeacherIdentity from '../../../components/teacher-identity/Teacher-identity.component';
import TeacherContact from '../../../components/teacher-contact/Teacher-contact.component';

function TeacherProfileView() {
    
    return (
      <Container>
        <Grid container spacing={3}>
            <Grid item xs={4} >
                <TeacherIdentity/>
            </Grid>
            <Grid item xs={8} >
                <TeacherContact />
            </Grid>
        </Grid>
      </Container>
    );
};

export default TeacherProfileView;
// const mapStateToProps = createStructuredSelector(
//   {
//     currentUser: selectCurrentUser
//   }
// );

// export default connect(mapStateToProps, null)(TeacherProfileView);