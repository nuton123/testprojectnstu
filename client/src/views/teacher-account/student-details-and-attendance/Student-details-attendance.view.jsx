import React from 'react';
import {Container, Grid } from '@material-ui/core';

import StudentIdentity from '../../../components/student-identity/Student-identity.component';

export default function StudentDetailsAttendance() {
  return (
    <Container>
        <Grid container spacing={3}>
            <Grid item xs={4} >
                <StudentIdentity />
            </Grid>
            <Grid item xs={8} >
               
            </Grid>
        </Grid>
    </Container>
  );
};