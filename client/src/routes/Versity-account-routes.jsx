import { Switch, Route, withRouter } from "react-router-dom";
import VersityDashboard from "../components/versity-dashboard/Versity-dashboard.component";
import VersityToken from "../components/versity-token/Versity-token.component";

function VersityAccountRoutes({ match }) {
  return (
    <Switch>
      <Route
        exact
        path={`${match.path}/dashboard`}
        component={VersityDashboard}
      />
      <Route
        exact
        path={`${match.path}/generate-token`}
        component={VersityToken}
      />
    </Switch>
  );
}

export default withRouter(VersityAccountRoutes);
