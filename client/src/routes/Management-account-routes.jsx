
import { Switch, Route, withRouter } from "react-router-dom";
import ManagementDashboard from "../components/management-dashboard/Management-dashboard.component";


function ManagementAccountRoutes({ match }) {
  return (
    <Switch>
       <Route exact path={`${match.path}/dashboard`} component={ManagementDashboard} />   
    </Switch>
  );

}

export default withRouter(ManagementAccountRoutes);
