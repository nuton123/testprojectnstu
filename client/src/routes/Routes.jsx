import * as React from "react";
import { Switch, Route } from "react-router-dom";
import AdminAccount from "../pages/AdminAccount";
import TeacherAccount from "../pages/TecherAccount";
import StudentAccount from "../pages/StudentAccount";
import SignIn from "../pages/Login";
import VersityAccount from "../pages/VersityAccount";
import ManagementAccount from "../pages/ManagementAccount";
import RegisterWithTokenForm from "../pages/RegisterWithTokenForm";
import Registration from "../pages/Registration";


function Router() {
  return (
    <div className="App">
      <Switch>
        <Route exact path="/" component={SignIn} />
        <Route exact path="/register" component={RegisterWithTokenForm} />
        <Route exact path="/register-form" component={Registration} />
        <Route path="/management" component={ManagementAccount} />
        <Route path="/teacher-account" component={TeacherAccount} />
        <Route path="/student-account" component={StudentAccount} />
        <Route path="/admin-account" component={AdminAccount} />
        <Route path="/versity-account" component={VersityAccount} />
      </Switch>
    </div>
  );
}

export default Router;
