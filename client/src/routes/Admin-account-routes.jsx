import AddNewTeachers from "../components/TeacherTable/AddTeachers";
import AddStudents from "../components/StudentTable/AddStudents";
import { Switch, Route, withRouter } from "react-router-dom";
import AdminDashboard from "../components/admin-dashboard/Admin-dashoard";
import StudentValidationRequest from "../components/admin-validation/student-validation-request/Student-validation-request.component";
import TeacherValidationRequest from "../components/admin-validation/teacher-validation-request/Teacher-validation-request.component";
import ValidStudentView from "../components/admin-validation/student-validation-request/Validate-student-view.component";
import ValidTeacherView from "../components/admin-validation/teacher-validation-request/Validate-teacher-view.component";

function AdminAccountRoutes({ match }) {
  return (
    <Switch>
      <Route exact path={`${match.path}/dashboard`} component={AdminDashboard} />
      <Route exact path={`${match.path}/teacher-validation-request`} component={TeacherValidationRequest} />
      <Route exact path={`${match.path}/teacher-validation-request/teacher-id1`} component={ValidTeacherView} />

      <Route exact path={`${match.path}/student-validation-request`} component={StudentValidationRequest} />
      <Route exact path={`${match.path}/student-validation-request/student-id1`} component={ValidStudentView} />

      <Route exact path={`${match.path}/add-student`} component={AddStudents} />
      <Route
        exact
        path={`${match.path}/add-teacher`}
        component={AddNewTeachers}
      />
    </Switch>
  );
}

export default withRouter(AdminAccountRoutes);
