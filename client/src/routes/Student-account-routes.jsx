import React from 'react';
import { Switch, Route,withRouter } from 'react-router-dom';

import StudentProfileView from '../views/student-account/student-profile/Student-profile.view'
import StudentAttendance from '../components/student-attendance/StudentAttendance';
import AllClassesAttendance from '../components/student-attendance/AllClassesAttendance';
import SemisterAttendanceData from '../components/student-attendance/SemisterAttendanceData';
import StudentDashboardView from '../views/student-account/student-dashboard/Student-dashboard.view';


function StudentAccountRoutes({match}) {
    return (
        <Switch>
            <Route exact path={`${match.path}/dashboard`} component={StudentDashboardView} />
            <Route exact path={`${match.path}/student/profile`} component={StudentProfileView} />
            <Route exact path={`${match.path}/student/my-attendance`} component={StudentAttendance} />
            <Route exact path={`${match.path}/student/my-all-classes`} component={AllClassesAttendance} />
            <Route exact path={`${match.path}/student/my-attendance/semister-1`} component={SemisterAttendanceData} />
        </Switch>
       
    );
};

export default withRouter(StudentAccountRoutes);
