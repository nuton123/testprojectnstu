import React from 'react';
import { Switch, Route,withRouter } from 'react-router-dom';

import TeacherProfileView from '../views/teacher-account/teacher-profile/Teacher-profile.view';
import StudentDetailsTable from '../components/student-details-table/Student-details-table.component';
import StudentDetailsAttendance from '../views/teacher-account/student-details-and-attendance/Student-details-attendance.view';
import CreateCourese from '../components/courses/CreateCourse.component';
import TeacherDashboard from '../views/teacher-account/teacher-dashoard/Teacher-dashboard.view';
import StudentAttendanceSheet from '../components/student-attendance-table/Student-attendance-sheed.component';
import CreateBatch from '../components/course-coordinator/CreateBatch.component';
import SingleStudentDetailsForm from '../components/student-details/Single-student-details-form.component';


function TeacherAccountRoutes({match}) {
    return (
        <Switch>
            <Route exact path={`${match.path}/dashboard`} component={TeacherDashboard} />
            <Route exact path={`${match.path}/teacher/profile`} component={TeacherProfileView} />
            <Route exact path={`${match.path}/teacher/attendance`} component={CreateCourese} />
            <Route exact path={`${match.path}/teacher/attendance/ice/ice4201/batch5`} component={StudentAttendanceSheet} />
            <Route exact path={`${match.path}/teacher/student-details`} component={StudentDetailsTable} />
            <Route exact path={`${match.path}/teacher/co-ordinator`} component={CreateBatch} />
            <Route exact path={`${match.path}/teacher/co-ordinator/ice/batch5`} component={StudentDetailsTable} />
            <Route exact path={`${match.path}/teacher/co-ordinator/ice/batch5/student-id-1`} component={SingleStudentDetailsForm} />
            <Route exact path={`${match.path}/teacher/chairman`} component={StudentDetailsAttendance} /> 
        </Switch>
      
    );
};

export default withRouter(TeacherAccountRoutes);
